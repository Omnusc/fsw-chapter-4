/* 
Buat fungsi untuk menghitung luas dan keliling lingkaran
*/
let r = 10;
function circleAreaRadius(r)
{
    const PHI = 3.14;
    let area = PHI * r * r;
    let radius = PHI * 2 * r;
    
    console.log(area);
    console.log(radius);
    return area, radius;
}

circleAreaRadius(r);
