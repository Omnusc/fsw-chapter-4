// #1
console.log("#1");
let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/
// console.log(fruits.length);
// this
function fruitLoop(fruits)
{
    // .length output is how many element, count from 1 - 6, not indexing 0-5
    for(let i = fruits.length - 1; i >= 0; i--)
    {
        console.log(fruits[i]);
    }
}
fruitLoop(fruits);

// #2
console.log("#2");
// we can use splice method in javascript
// .splice(1st arg, 2nd 2arg, 3rd arg)
// 1st arg = which starting index we want to add
// 2nd arg = which index we want to delete
// 3rd arg = data we want to put
let month = [
  "January", // 0
  "February", // 1
  "July", // 2
  "August",
  "September",
  "October",
  "November",
  "December",
];

// Input element inside array
month.splice(2,0, "March", "April", "May", "June");
month.forEach(function(month)
{
    console.log(month);
})
// for(const test of month)
// {
//     console.log(test);
// }

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/


// #3
console.log("#3");
const message1 = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

let indexStart = message1.indexOf("belajar") // 23
// console.log(indexStart);
function takeWord(indexStart, message1)
{
    let newMsg = message1.substring(indexStart, message1.length);
    console.log(newMsg);
    return newMsg;
}
let newMsg1 = takeWord(indexStart, message1);
/* 
Output: belajar Javascript menyenangkan
*/

// #4
console.log("#4");
const message2 = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";
/* 
Output: Belajar Javascript menyenangkan
*/
// to uppercase doesnt accept any arg
// the charAt(0) output is 'b'
let capitalWord = newMsg1.charAt(0).toUpperCase();
// slice = start printing the newMsg1 from the 1st index which is 'e'
let capitalSentece = capitalWord + newMsg1.slice(1);
console.log(capitalSentece);

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/
console.log("#5");
let massSteven = 78;
let heightSteven = 1.69;

let massBill = 92;
let heightBill = 1.95;
function BMI(mass, height)
{
    let bmi = mass / (height * height);
    return bmi;
}
let bmiSteven = BMI(massSteven, heightSteven);
let bmiBill = BMI(massBill, heightBill);
console.log(`Steven BMI: ${bmiSteven} and Bill BMI: ${bmiBill}`);
// BMI(mass,height);


/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/
console.log("#6");
let johnWeight = 95;    
let johnHeight = 1.88;
let nashWeight = 85;
let nashHeight = 1.76;
let johnBMI = BMI(johnWeight, johnHeight);
let nashBMI = BMI(nashWeight, nashHeight);

if(johnBMI > nashBMI)
{
    console.log(`John's BMI (${johnBMI}) is higher than Nash's (${nashBMI})`)
}
else if(johnBMI < nashBMI)
{
    console.log(`John's BMI (${johnBMI}) is lower than Nash's (${nashBMI})`);
}


//  #7 Looping
console.log("#7");
let data = [10, 20, 30, 40, 50];
let total;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/
function totalArray(data)
{
    let total = 0;
    for(let i = 0; i < data.length ; i++)
    {
        total += data[i];
    }
    return total;
}
total = totalArray(data);

console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/